var tips = receiveData(setupTips);

function receiveData(callback){
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = false;
    xhr.open("GET", "https://rsv-leadersofdigital.herokuapp.com/tips", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send();
    xhr.onreadystatechange = (e) => {
         if(typeof (xhr.responseText) !== 'undefined' && xhr.responseText !== '')
         {
            var json = xhr.responseText;
            callback(json);
         }
      }
 }

 //WARGNING: this is the prototype, testing
function setupTips(tips){
    var json = JSON.parse(tips);
    var css = json.css + '';
    var script = json.script;
    
    var extc = css.split(',');
    extc.forEach((ext) => {
        var ecss = document.createElement('link');
        ecss.href = ext;
        ecss.rel = "stylesheet";
        document.head.appendChild(ecss);
    });

    var tipScript = document.createElement('script');
    tipScript.type = "text/javascript";
    tipScript.text = script;
    document.body.appendChild(tipScript);
}

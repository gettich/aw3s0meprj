chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    if(typeof changeInfo.url !== 'undefined')
    {
        if(changeInfo.url.match('rsv.ru/*'))
            sendData(tab.url);
    }
 }); 
 
 chrome.tabs.onActivated.addListener(function(activeInfo) {
   chrome.tabs.get(activeInfo.tabId, function(tab){
       if(tab.url.match('rsv.ru/*'))
            sendData(tab.url);
   });
 }); 

 function sendData(url){
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = false;
    xhr.open("POST", "https://rsv-leadersofdigital.herokuapp.com/action", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
        uri: url,
    }));
 }
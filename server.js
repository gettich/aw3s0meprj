const express = require("express");
const bodyParser = require("body-parser")
const ejs = require("ejs");
const cors = require("cors");
const app = express();
const path = require('path')

app.set("view engine","ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json())
app.use(cors());

var links = {};
var script = 'Нет активного скрипта';
var css = 'Нет активной разметки';

app.post("/action", (req, res)=>{
    var found = false;
    for (var key in links)
    {
        if(key === req.body.uri)
        {
            found = true;
            links[key]++;
        }
    }
    if(!found)
        links[req.body.uri] = 1;
    res.redirect("/");
});

app.get("/", (req,res)=>{
    var mlinks = Object.keys(links);
    var data = Object.values(links);
    res.render("index", {mlinks: mlinks, data: data, links: mlinks});
});

app.get("/tips", (req,res) => {
    res.send(JSON.stringify({
        script: script,
        css:css,
    }));
});

app.get("/manage", (req,res) => {
    res.render("manage", {script: script, css: css});
});

app.post("/new", (req,res) => {
    script = req.body.Code;
    css = req.body.css.split('\n');
    res.redirect("/manage");
});

app.post("/question", (res,req) => {

});

app.listen(process.env.PORT || 3000);